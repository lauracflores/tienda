package com.msacademy.test.tienda.repository;

import com.msacademy.test.tienda.entity.Producto;

public interface ProductoRepository extends org.springframework.data.jpa.repository.JpaRepository<Producto, Long>{
    public Producto findById(int id);
}
