package com.msacademy.test.tienda.service;

import com.msacademy.test.tienda.controller.ProductoException;
import com.msacademy.test.tienda.entity.Producto;
import com.msacademy.test.tienda.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoServiceImpl implements ProductoService{
    @Autowired
    ProductoRepository productoRepository;
    public Producto addProducto(Producto producto){
        return productoRepository.save(producto);
    }
    public Producto getProductoById(int id){
        Optional<Producto> productoOb = Optional.ofNullable(this.productoRepository.findById(id));
        if(!productoOb.isPresent())
            throw new ProductoException("A record with that ID does not exist");
        return productoOb.get();
    }
}
