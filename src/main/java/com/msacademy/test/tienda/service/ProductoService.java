package com.msacademy.test.tienda.service;

import com.msacademy.test.tienda.entity.Producto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ProductoService {
     Producto getProductoById(int id);
     Producto addProducto(Producto producto);
}
