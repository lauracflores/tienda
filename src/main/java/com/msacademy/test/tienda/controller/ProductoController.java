package com.msacademy.test.tienda.controller;

import com.msacademy.test.tienda.entity.Producto;
import com.msacademy.test.tienda.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ProductoController {
    @Autowired
    private ProductoRepository productoRepository;
    @GetMapping(value="/producto/{id}", produces="application/json")
    public Producto producto(@PathVariable int id){
        return productoRepository.findById(id);
    }

    @PostMapping(value="/producto", consumes="application/json")
    public ResponseEntity addProducto(@Valid @RequestBody Producto producto){
        return new ResponseEntity(productoRepository.save(producto), HttpStatus.OK);
    }
}
