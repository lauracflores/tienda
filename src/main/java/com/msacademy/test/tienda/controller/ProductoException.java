package com.msacademy.test.tienda.controller;

public class ProductoException extends RuntimeException{
    public ProductoException(String exception){
        super(exception);
    }
}
